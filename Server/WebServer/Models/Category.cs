﻿using System;
namespace WebServer.Models
{
  /// <summary>
  /// Категория продуктов.
  /// </summary>
  public class Category
  {
    /// <summary>
    /// Id.
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// Имя.
    /// </summary>
    public string Name { get; set; }
  }
}