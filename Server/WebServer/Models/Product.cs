﻿using System;

namespace WebServer.Models
{
  /// <summary>
  /// Продукт.
  /// </summary>
  public class Product:IEquatable<Product>
  {
    /// <summary>
    /// Id.
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// Имя.
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Стоимость.
    /// </summary>
    public double Cost { get; set; }
    /// <summary>
    /// Категория.
    /// </summary>
    public Category Category { get; set; }
    /// <summary>
    /// Ссыка на фото.
    /// </summary>
    public string Url { get; set; }

    /// <summary>
    /// Товар в корзине.
    /// </summary>
    public bool IsInBacket { get; set; } = false;

    /// <summary>
    /// Сравнение.
    /// </summary>
    /// <param name="other">Продукт.</param>
    /// <returns>Результат.</returns>
    public bool Equals(Product other)
    {
      if(this.Id.Equals(other.Id))
      { return true; }
      return false;
    }
  }
}