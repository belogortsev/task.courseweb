﻿using System;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using WebServer.Models;
using WebServer.Service;

namespace WebServer.Controllers
{
  /// <summary>
  /// Контроллер корзины.
  /// </summary>
  [Route("backet/{action}")]
  public class BacketController : ApiController
  {
    /// <summary>
    /// Доьавить продукт в корзину.
    /// </summary>
    /// <param name="id">Id продукта.</param>
    [HttpPost]
    public void Add([FromBody] IdRequest request)
    {
      new BacketServicet().Add(request.id);
    }

    /// <summary>
    /// Удалить продукт из корзины.
    /// </summary>
    /// <param name="id">Id продукта.</param>
    [HttpPost]
    public void Remove([FromBody] IdRequest request)
    {
      new BacketServicet().Remove(request.id);
    }

    /// <summary>
    /// Получить список продуктов в корзине.
    /// </summary>
    [HttpGet]
    public IHttpActionResult GetProducts()
    {
      var result = new BacketServicet().GetProducts();
      return Json(result);
    }
    public class IdRequest
    {
      public Guid id { get; set; }
    }

  }

  
}
