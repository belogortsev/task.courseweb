﻿using System;
using System.Collections.Generic;

using System.Web.Http;
using WebServer.Models;
using WebServer.Service;

namespace WebServer.Controllers
{
  /// <summary>
  /// Контроллер продуктов.
  /// </summary>
  [Route("categories/{action}")]
  public class CategoriesController : ApiController
  {
    /// <summary>
    /// Запрос списка продуктов по категории.
    /// </summary>
    /// <returns>Список продуктов.</returns>
    [HttpGet]
    public IHttpActionResult GetProducts([FromUri] String Name)
    {
      var result = new CategoriesService().GetProducts(Name);
      return Json(result);
    }
    /// <summary>
    /// Запрос списка категорий.
    /// </summary>
    /// <param name="id"></param>
    /// <returns>Список категорий.</returns>
    [HttpGet]
    public IHttpActionResult GetCategories()
    {
      var result = new CategoriesService().GetCategories();
      return Json(result);
    }


    
  }
}