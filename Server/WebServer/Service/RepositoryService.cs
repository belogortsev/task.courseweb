﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebServer.Models;

namespace WebServer.Service
{
  public class RepositoryService
  {

    #region Поля и свойства.
    /// <summary>
    /// Продукты.
    /// </summary>
    private static readonly IList<Product> products = new List<Product>();
    /// <summary>
    /// Категории.
    /// </summary>
    private static readonly IList<Category> categories = new List<Category>();

    /// <summary>
    /// Категории.
    /// </summary>
    public IList<Category> Categories { get { return categories; } }
    /// <summary>
    /// Продукты.
    /// </summary>
    public IList<Product> Products { get { return products; } }
    #endregion

    #region Методы

    #endregion
    #region Конструкторы
    /// <summary>
    /// Статический конструктор.
    /// </summary>
    static RepositoryService()
    {
      categories.Add(new Category { Id = Guid.NewGuid(), Name = "Холодильники" });
      categories.Add(new Category { Id = Guid.NewGuid(), Name = "Телвизоры" });
      categories.Add(new Category { Id = Guid.NewGuid(), Name = "Блендеры" });

      products.Add(new Product { Id = Guid.NewGuid(), Name = "Телевизор1", Category = categories.First(t => t.Name == "Телвизоры"), Cost = 1000d, Url = "https://img.icons8.com/ios/452/retro-tv.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Телевизор2", Category = categories.First(t => t.Name == "Телвизоры"), Cost = 1100d, Url = "https://img.icons8.com/ios/452/retro-tv.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Телевизор3", Category = categories.First(t => t.Name == "Телвизоры"), Cost = 2000d, Url = "https://img.icons8.com/ios/452/retro-tv.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Телевизор4", Category = categories.First(t => t.Name == "Телвизоры"), Cost = 3000d, Url = "https://img.icons8.com/ios/452/retro-tv.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Телевизор5", Category = categories.First(t => t.Name == "Телвизоры"), Cost = 3200d, Url = "https://img.icons8.com/ios/452/retro-tv.png" });

      products.Add(new Product { Id = Guid.NewGuid(), Name = "Холодильник1", Category = categories.First(t => t.Name == "Холодильники"), Cost = 1000d, Url = "https://img.icons8.com/ios/452/fridge.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Холодильник2", Category = categories.First(t => t.Name == "Холодильники"), Cost = 1100d, Url = "https://img.icons8.com/ios/452/fridge.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Холодильник3", Category = categories.First(t => t.Name == "Холодильники"), Cost = 2000d, Url = "https://img.icons8.com/ios/452/fridge.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Холодильник4", Category = categories.First(t => t.Name == "Холодильники"), Cost = 3000d, Url = "https://img.icons8.com/ios/452/fridge.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Холодильник5", Category = categories.First(t => t.Name == "Холодильники"), Cost = 3200d, Url = "https://img.icons8.com/ios/452/fridge.png" });

      products.Add(new Product { Id = Guid.NewGuid(), Name = "Блендер1", Category = categories.First(t => t.Name == "Блендеры"), Cost = 100d, Url = "https://img.icons8.com/ios/452/blender.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Блендер2", Category = categories.First(t => t.Name == "Блендеры"), Cost = 1100d, Url = "https://img.icons8.com/ios/452/blender.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Блендер3", Category = categories.First(t => t.Name == "Блендеры"), Cost = 1200d, Url = "https://img.icons8.com/ios/452/blender.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Блендер4", Category = categories.First(t => t.Name == "Блендеры"), Cost = 1300d, Url = "https://img.icons8.com/ios/452/blender.png" });
      products.Add(new Product { Id = Guid.NewGuid(), Name = "Блендер5", Category = categories.First(t => t.Name == "Блендеры"), Cost = 3200d, Url = "https://img.icons8.com/ios/452/blender.png" });     
    }
    #endregion

  }
}