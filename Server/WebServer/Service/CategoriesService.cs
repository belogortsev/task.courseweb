﻿using System.Collections.Generic;
using System.Linq;
using WebServer.Models;

namespace WebServer.Service
{
  /// <summary>
  /// Сервис категории продуктов.
  /// </summary>
  public class CategoriesService
  {
    #region Поля и свойства.

    #endregion

    #region Методы.
    /// <summary>
    /// Получить категории.
    /// </summary>
    /// <returns>Список категорий.</returns>
    public IEnumerable<Category> GetCategories()
    {
      return new RepositoryService().Categories;
    }
    /// <summary>
    /// Получить список продуктов по категории
    /// </summary>
    /// <param name="category"></param>
    /// <returns></returns>
    public IEnumerable<Product> GetProducts(string categoryName)
    {
     return new RepositoryService().Products.Where(t => t.Category.Name.Equals(categoryName)).ToList();
    }
    #endregion

    #region Конструкторы.

    #endregion
  }
}