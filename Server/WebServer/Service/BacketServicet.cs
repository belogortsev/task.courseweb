﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebServer.Models;

namespace WebServer.Service
{
  public class BacketServicet
  {
    #region Поля и свойства.
    /// <summary>
    /// Корзина.
    /// </summary>
    private static IList<Product> products = new List<Product>();
    #endregion
    #region Методы.
    /// <summary>
    /// Добавить продукт в корзину.
    /// </summary>
    /// <param name="id">Id продукта.</param>
    public void Add(Guid id)
    {
     var result = new RepositoryService().Products.FirstOrDefault(t => t.Id.Equals(id));
      if (result != null) {
        result.IsInBacket = true;
      }      
    }

    /// <summary>
    /// Удалить продукт из корзины.
    /// </summary>
    /// <param name="id">Id продукта.</param>
    public void Remove(Guid id)
    {
     var result =  new RepositoryService().Products.FirstOrDefault(t => t.Id.Equals(id));
      if (result != null)
      {
        result.IsInBacket = false;
      }      
    }
    /// <summary>
    /// Получить все продукты в корзине.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Product> GetProducts()
    {
      return new RepositoryService().Products.Where(t => t.IsInBacket).ToList();
    }
    #endregion

    #region Конструкторы.

    #endregion
  }
}