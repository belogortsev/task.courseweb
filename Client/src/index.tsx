import * as React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Shop from './components/shop/shop';

require('./styles/app.css');

render(
  <BrowserRouter>
    <Shop/>
  </BrowserRouter>  ,
  document.getElementById('root')
);
