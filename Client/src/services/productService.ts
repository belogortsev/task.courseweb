import { IProductCategory } from '../types/productCategory';
import { IProduct } from '../types/product';

import { getJson } from './httpClient';

/**
 * Получить список категорий.
 */
export async function getProductCategories(): Promise<Array<IProductCategory>> {
  return getJson<null, Array<IProductCategory>>('http://localhost:1000/api/Categories/GetCategories', null);
}
/**
 * Получить список продуктов по категории.
 * @param categoryName Имя категории.
 */
export async function getProducts(categoryName: string): Promise<Array<IProduct>> {
  return getJson<{ Name: string }, Array<IProduct>>('http://localhost:1000/api/Categories/GetProducts', { Name: categoryName });
}