import { IProduct } from '../types/product';

import { getJson, postJson } from './httpClient';
/**
 * Получить список продуктов в корзине.
 */
export async function getBacket(): Promise<Array<IProduct>> {
  return getJson('http://localhost:1000/api/Backet/GetProducts');
}
/**
 * Добавить продукт в корзину.
 * @param product Продукт котороый добавляем.
 */
export async function addProductToBacket(product: IProduct): Promise<boolean> {
  await postJson('http://localhost:1000/api/Backet/Add', { id: product.Id });
  return true;
}
/**
 * Удалить продукт из корзины.
 * @param product Продукт который удаляем.
 */
export async function removeProductFromBacket(product: IProduct): Promise<boolean> {
  await postJson('http://localhost:1000/api/Backet/Remove', { id: product.Id });
  return true;
}