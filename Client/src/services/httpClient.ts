import axios from 'axios';

/**
 * Сделать get  запрос.
 * @param url Адрес запроса.
 * @param params Параметры для передачи.
 */
export async function getJson<TIn, TOut>(url: string, params?: TIn): Promise<TOut> {

  const result = await axios.get(url, { params });
  return result.data;
}
/**
 * Сделать post  запрос.
 * @param url Адрес запроса.
 * @param params Параметры для передачи.
 */
export async function postJson<TIn, TOut>(url: string, params?: TIn): Promise<TOut> {

  const headers = { 'Content-Type': 'application/json' };
  const result = await axios.post(url, params, { headers: headers });
  return result.data;
}