import React from 'react';
import { match } from 'react-router';

import { getProducts } from '../../services/productService';
import { IProduct } from '../../types/product';
import { Product } from '../product/product';

import './products.css';

/**
 * Props.
 */
export interface IProps {

  /**
   * Объект для доступа к базовым свойствам.
   */
  match: match<{ name: string }>;
}
/**
 * State.
 */
export interface IState {
  /**
   * Категория.
   */
  categoryName: string;
  /**
   * Список продуктов.
   */
  products: Array<IProduct>;
}

/**
 * Компонент отображающий продукты.
 */
export class Products extends React.Component<IProps, IState> {

  public async componentDidMount() {
    this.updateComponent();
  }

  public componentDidUpdate() {

    if (this.props.match.params.name !== this.state.categoryName) {
      this.updateComponent();
    }

  }
  /**
   * Получение свойств и перезапрос сведений о продукте.
   */
  public async updateComponent() {

    const categoryName = this.props.match.params.name;
    const products = await getProducts(categoryName);

    this.setState({
      categoryName: categoryName,
      products: products
    });
  }
  public render() {
    if (this.state == null || this.state.categoryName == null) {
      return (<div>Выберите категорию!</div>);
    }
    const elementHeader = <div>{this.state.categoryName}</div>;
    const elementsBody = this.state.products.map(product => <div key={product.Id} className='products__item'><Product Product={product} /></div>);

    return (
      <div>
        <div>{elementHeader}</div>
        <div className='products'>{elementsBody}</div>
      </div>);
  }
}
