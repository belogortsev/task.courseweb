import React from 'react';
import { Link } from 'react-router-dom';

import { IProductCategory } from '../../types/productCategory';

/**
 * Props
 */
export interface IProps {
  /**
   * Категория.
   */
  Category: IProductCategory;
}
/**
 * States
 */
export interface IState {

}
/**
 * Комопонент конкретной категории.
 */
export class Category extends React.Component<IProps, IState> {
  public render() {
    const name = this.props.Category.Name;
    const path = `/category/${name}`;
    return (<div><Link to={{ pathname: path, state: this.props.Category }}>{name}</Link></div >);
  }
}
