import React from 'react';

import { IProductCategory } from '../../types/productCategory';
import { getProductCategories } from '../../services/productService';
import { Category } from '../category/category';

import './categories.css';


/**
 * Props.
 */
export interface IProps { }
/**
 * State.
 */
export interface IState {
  /**
   * Список категорий.
   */
  Categories: Array<IProductCategory>;
}
/**
 * Компонент категорий товаров.
 */
export class Categories extends React.Component<IProps, IState> {

  public async componentDidMount() {
    const categories = await getProductCategories();
    this.setState({ Categories: categories });
  }

  public render(): React.ReactNode {
    if (this.state != null && this.state.Categories != null) {
      const viewResult = this.state.Categories.map(category => <Category key={category.Id} Category={category} />);
      return (<div className='categories'>{viewResult}</div>);
    }
    return (<div />);
  }
}
