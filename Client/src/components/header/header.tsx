import React from 'react';
import { Link } from 'react-router-dom';

import './header.css';

/**
 * Верхняя панель.
 */
export default class Header extends React.Component {

  public render() {
    const pathToBacket = '/backet';
    return (
      <div className='header'>
        <img className='logo' src='https://avatanplus.com/files/resources/original/57b959ab8d9ec156ac06463c.png' width='50' height='50' />
        <Link to={ pathToBacket} className='logo'>
          <img src='https://img.icons8.com/ios/452/shopping-cart.png' height='50' width='50' />
        </Link>
      </div>);
  }
}
