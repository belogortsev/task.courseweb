import React, { Component } from 'react';

import Explorer from '../explorer/explorer';
import Header from '../header/header';
import MainContent from '../mainContent/mainContent';

import './shop.css';
/**
 * Комопнент магазина.
 */
export default class Shop extends Component {

  public render() {
    return (
      <div className='shop'>
        <div className='shop__header'>
          <Header />
        </div>
        <div className='shop__main'>
          <div className='shop__explorer'>
            <Explorer />
          </div>
          <div className='shop__content'>
            <MainContent />
          </div>
        </div>
      </div>
    );
  }
}