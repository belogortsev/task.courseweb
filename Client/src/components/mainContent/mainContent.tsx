import React from 'react';
import { Route } from 'react-router-dom';

import { Backet } from '../backet/backet';
import { Products } from '../products/products';

/**
 * Центральная панель.
 */
export default class MainContent extends React.Component {

  public render() {
    return (
      <div>
        <Route path='/category/:name' component={Products} />
        <Route path='/backet' component={Backet} />
      </div>);
  }
}