import React from 'react';
import { autobind } from 'core-decorators';

import { getBacket } from '../../services/backetService';
import { IProduct } from '../../types/product';
import { Product } from '../product/product';

import './backet.css';

/**
 * Props.
 */
export interface IProps {
}
/**
 * State.
 */
export interface IState {
  /**
   * Список продуктов.
   */
  Products: Array<IProduct>;
}
/**
 * Корзина.
 */
@autobind
export class Backet extends React.Component<IProps, IState> {

  public async componentDidMount() {

    const products = await getBacket();
    this.setState({ Products: products });
  }

  /**
   * Удалить продукт из списка.
   * @param product Продукт.
   */
  public removeProduct(product: IProduct) {
    const products = this.state.Products.filter(p => p.Id !== product.Id);
    this.setState({ Products: products });
  }

  public render() {
    if (this.state == null || this.state.Products == null) {
      return (<div>Корзина</div>);
    }
    const productsView = this.state.Products.map(t => <div key={t.Id} className='item'><Product Product={t} RemoveFromBucketEvent={this.removeProduct} /> </div>);
    return (
      <div>
        <div>Корзина</div>
        <div className='backet'>{productsView}</div>
      </div>);
  }
}
