import React from 'react';

import { addProductToBacket, removeProductFromBacket } from '../../services/backetService';
import { IProduct } from '../../types/product';

import './product.css';

/**
 * Props.
 */
export interface IProps {
  /**
   * Продукт.
   */
  Product: IProduct;
  /**
   * Действие удалить из корзины.
   */
  RemoveFromBucketEvent?: (product: IProduct) => void;
}
/**
 * State.
 */
export interface IState {
  /**
   * Продукт.
   */
  Product: IProduct;
}
/**
 * Компонент отображающий продукт.
 */
export class Product extends React.Component<IProps, IState> {

  /**
   * Конструктор.
   * @param props Props.
   */
  constructor(props: IProps) {
    super(props);
    this.state = { Product: this.props.Product };

    this.removeFromBacket = this.removeFromBacket.bind(this);
    this.addToBacket = this.addToBacket.bind(this);
  }

  /**
   * Удалить из корзины.
   */
  private async removeFromBacket() {
    const result = await removeProductFromBacket(this.props.Product);
    if (result) {
      const product = this.state.Product;
      product.IsInBacket = false;
      this.setState({ Product: product });
      if (this.props.RemoveFromBucketEvent != null) {

        this.props.RemoveFromBucketEvent(product);
      }
    }
  }

  /**
   * Добавить в корзину.
   */
  private async addToBacket() {
    const result = await addProductToBacket(this.props.Product);
    if (result) {
      const product = this.state.Product;
      product.IsInBacket = true;
      this.setState({ Product: product });
    }
  }

  public render() {
    const elementButton = this.state.Product.IsInBacket ?
      <button onClick={this.removeFromBacket}>Удалить из корзины</button> :
      <button onClick={this.addToBacket}>Добавить в корзину</button>;
    return (
      <div className='product product__opacity'>
        <img src={this.state.Product.Url} width='50' height='70' />
        <div className='product__name'>{this.state.Product.Name}</div>
        <div className='product__cost'>{this.state.Product.Cost}</div>
        <div className='product__category'>{this.state.Product.Category.Name}</div>
        {elementButton}
      </div>
    );
  }

}