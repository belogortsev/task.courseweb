import React from 'react';

import { Categories } from '../categories/categories';

import './explorer.css';
/**
 * Левая панель.
 */
export default class Explorer extends React.Component {

  public render() {
    return (<div className='explorer'><Categories/></div>);
  }
}
