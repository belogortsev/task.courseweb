import { IProductCategory } from './productCategory';

export interface IProduct {
  Id: string;
  Category: IProductCategory;
  Name: string;
  Cost: number;
  Url: string;
  IsInBacket: boolean;
}